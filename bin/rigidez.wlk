

mixin Rigida {
	var porcentajeDeAbsorcion = 0.5
	method danioATransmitir(cuanto) = cuanto * porcentajeDeAbsorcion
}

mixin SemiRigida {
	var puntosDeAbsorcion = 10
	
	method danioATransmitir(cuanto) = cuanto - puntosDeAbsorcion
}