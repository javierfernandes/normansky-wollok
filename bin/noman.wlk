
class Personaje {
	var planeta
	var mochila
	
	method planeta() = planeta
	method planeta(_planeta) { planeta = _planeta }
	method mochila(m) { mochila = m }
	
	method capacidadLibreVolumen() = mochila.capacidadLibreVolumen()
	method cargar(e) { mochila.agregar(e) }
}



