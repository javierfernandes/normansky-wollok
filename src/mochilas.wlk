mixin Contenedor {
	method capacidadLibreVolumen()
	
	method agregar(elemento)
	method remover(elemento)
	
	method elementos()
}

class Mochila mixed with Contenedor {
	const cargaMaxima
	const elementos = []
	constructor(_cargaMaxima) {
		cargaMaxima = _cargaMaxima
	}
	
	override method elementos() = elementos
	
	override method agregar(elemento) { elementos.add(elemento) }
	override method remover(elemento) { elementos.remove(elemento) }
	
	override method capacidadLibreVolumen() = cargaMaxima - self.cargaActual()
	
	method cargaActual() = elementos.sum { e => self.volumenOcupadoPor(e) }
	
	method volumenOcupadoPor(e) = e.volumen()
	
	method daniar(cuanto) {
		elementos.forEach { e => e.daniar(self.danioATransmitir(cuanto)) }	
	}
	method danioATransmitir(cuanto) = cuanto
}

class MochilaChica inherits Mochila {
	constructor() = super(40000)
}

class MochilaMediana inherits Mochila {
	constructor() = super(60000)
}
class MochilaGrande inherits Mochila {
	constructor() = super(90000)
}