
class Elemento {
	var danio = 0
	method volumen()
	method volumenCompactado() = self.volumen()
	method volumenDeshidratado() = self.volumen()
	method danio() = danio
	method daniar(cuanto) { danio += cuanto }
}

// ********************
// ** compactacion
// ********************

mixin Compactable {
	method volumenCompactado() = self.volumen() / 2
	method volumen()
}

mixin SemiCompactable {
	method volumenCompactado() = self.volumen() - self.reduccionPorCompactacion()
	
	method reduccionPorCompactacion()
	method volumen()
}

// ********************
// ** Organicos
// ********************

mixin Organico {
	method volumenDeshidratado() = self.volumen() - self.volumenDeAgua()
	
	method volumenDeAgua()
	method volumen()
}

// ********************
// ** objetos ejemplo
// ********************

object roca inherits Elemento {
	override method volumen() = 200
}

object diario inherits Elemento mixed with Compactable {
	override method volumen() = 500
}

object caja inherits Elemento mixed with SemiCompactable {
	override method volumen() = 400
	override method reduccionPorCompactacion() = 100 
}

object tomate inherits Elemento mixed with Organico {
	override method volumen() = 250
	override method volumenDeAgua() = self.volumen() * 0.9
}
