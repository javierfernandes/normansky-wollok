
mixin CompactadorPorVacio {
	method volumenOcupadoPor(e) = e.volumenCompactado()
}

mixin CompactadorPorDeshidratacion {
	method volumenOcupadoPor(e) = e.volumenDeshidratado()
}
